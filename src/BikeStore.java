//Parhomenco Kirill 1933830
public class BikeStore {
    public static void main(String[] args) {
        System.out.println("We greet you in our bicycle store!");
        Bicycle[] myDream = new Bicycle[4];
        myDream[0] = new Bicycle( "Trek", 11, 30);
        myDream[1] = new Bicycle( "Colnago", 22, 55);
        myDream[2] = new Bicycle( "S-Works", 10, 30);
        myDream[3] = new Bicycle( "Canyon", 24, 60);
        for (Bicycle bike: myDream
             ) {
            System.out.println(bike);
        }
    }
}
