//Parhomenco Kirill 1933830
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears +
                ", Maximum speed " + this.maxSpeed;
    }
}
